Synopsis
--------

<!---
        At the top of the file there should be a short introduction and/ or overview that explains **what** the project is. This description should match descriptions added for package managers (Gemspec, package.json, etc.)
--->

This repository is a public place for me to store my talks that I have given or plan to give

Motivation
----------

<!---
        A short description of the motivation behind the creation and maintenance of the project. This should explain **why** the project exists.
--->

This repo allows me to publicly store my talks somewhere such that I can access them from any location, but also to provide others with access to the talks that I have given

Installation
------------

<!---
        Provide code examples and explanations of how to get the project.
--->

I'm currently learning how to write the talks in latex, once I've grasped this I will update this readme with details as to how to build the .tex files if you don't want to just download the releases

Contributors
------------

<!---
        Let people know how they can dive into the project, include important links to things like issue trackers, irc, twitter accounts if applicable.
--->

I am the only contributor to this repo, as it is entirely my own work, any other cases are labelled.

License
-------

<!---
        A short snippet describing the license (MIT, Apache, etc.)
--->

Please feel free to use this work as you see fit, though accreditation would be appreciated! http://www.wtfpl.net/
